<?php 

function wpbootstrap_scripts()
{
    wp_enqueue_script('jquery');


    
    wp_enqueue_script( 'bootstrap', plugins_url('/js/bootstrap.js', __FILE__), array('jquery'));   
    wp_enqueue_style( 'style', plugins_url('/css/bootstrap.css', __FILE__));
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts' );

// function themeslug_enqueue_style() {
//     wp_enqueue_style( 'core', 'style.css', false ); 
// }

// function themeslug_enqueue_script() {
//     wp_enqueue_script( 'my-js', '/js/bootstrap.js', false );
// }

// add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_style' );
// add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_script' );


function team_member() {
    register_post_type( 'team_member_reviews',
        array(
            'labels' => array(
                'name' => 'Team Members',
                'singular_name' => 'Team Member Review',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Team Member',
                'edit' => 'Edit',
                'edit_item' => 'Edit Team Member Review',
                'new_item' => 'New Team Member Review',
                'view' => 'View',
                'view_item' => 'View Team Member Review',
                'search_items' => 'Search Team Member Reviews',
                'not_found' => 'No Team Member found',
                'not_found_in_trash' => 'No Movie Team Member found in Trash',
                'parent' => 'Parent Team Member Review'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( '' ),
            // 'menu_icon' => plugins_url( 'img/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

add_action( 'init', 'team_member' );

add_filter( 'rwmb_meta_boxes', 'team_member_meta_boxes' );

function team_member_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Other Information', 'textdomain' ),
        'post_types' => 'team_member_reviews',
        'fields'     => array(
            array(
                'id'   => 'position',
                'name' => __( 'Position', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'email',
                'name' => __( 'Email', 'textdomain' ),
                'type' => 'email',
            ),
            array(
                'id'   => 'phone',
                'name' => __( 'Phone', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'website',
                'name' => __( 'Website', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => "image",
                'name' => __( 'Image', 'textdomain' ),
                'type' => 'image',
            ),
        ),
    );
    return $meta_boxes;
}


function team_member_shortcode(){
    ob_start();
    $args = array(
        'post_type' => 'team_member_reviews',
    );
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $imageId = get_post_meta(get_the_ID(), 'image', true);

            ?>
                <br>
                <div class="col-lg-4">
                    <?php echo wp_get_attachment_image($imageId , $size= 'thumbnail', $icon = false, $attr= '');?><br>
                    <strong><?php echo get_the_title();?></strong><br>
                    <?php echo get_post_meta(get_the_ID(), 'position', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'email', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'phone', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'website', true) ;?><br>
                </div>
                <br>
            <?php

        }
        wp_reset_postdata();
    } else {
        echo  "no posts found";
    }

    return ob_get_clean();
}

add_shortcode('team_member_name', 'team_member_shortcode');

function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init' );

class RandomPostWidget extends WP_Widget
{
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'RandomPostWidget', 
            'description' => 'Displays a random post'
        );
        parent::__construct( 'RandomPostWidget', 'My Widget', $widget_ops );
    }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
    ?>
      <p>
      <label for="<?php echo $this->get_field_id('title'); ?>">Title: 
        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
      </label>
      </p>
    <?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
    $args = array(
        'post_type' => 'team_member_reviews',
    );
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $imageId = get_post_meta(get_the_ID(), 'image', true);

            ?>
                <br>
                <div>
                    <?php echo wp_get_attachment_image($imageId , $size= 'thumbnail', $icon = false, $attr= '');?><br>
                    <strong><?php echo get_the_title();?></strong><br>
                    <?php echo get_post_meta(get_the_ID(), 'position', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'email', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'phone', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'website', true) ;?><br>
                </div>
                <br>
            <?php

        }
        wp_reset_postdata();
    } else {
        echo  "no posts found";
    }


 
    echo $after_widget;
  }
 
}

// register Foo_Widget widget
function register_foo_widget() {
    register_widget( 'RandomPostWidget' );
}
add_action( 'widgets_init', 'register_foo_widget' );


// register_nav_menus( array(  
//   'primary' => __( 'Primary Navigation', 'twentyten' ),  
//   'secondary' => __('Secondary Navigation', 'twentyten')  
// ) );


function register_my_menus() {
    register_nav_menus(
        array(
          'header-menu' => __( 'Header Menu' ),
          'extra-menu' => __( 'Footer Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );


?>