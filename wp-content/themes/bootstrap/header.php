<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


        <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    	<?php wp_head(); ?>

    </head>

    <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
    <br><br><br>
    	<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>

    </div>
    	
    
<!--     <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
			  <div class="container">
			    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			      <span class="icon-bar"></span>
			    </a>

			    <a class="brand" href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>

			    <div class="nav-collapse collapse">
			    <br><br>
			      <ul class="nav">
			      	
			      

			      </ul>
			    </div>

			  </div>
			</div>
		</div>
 -->




		<!-- <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
			  <div class="container">
			    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </a>

			    <a class="brand" href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>

			    <div class="nav-collapse collapse">
			    <br><br>
			      <ul class="nav">

			      	<li class="active"><a href="#">Home</a></li>
		              <li><a href="#about">About</a></li>
		              <li><a href="#contact">Contact</a></li>
		              <li class="dropdown">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
		                <ul class="dropdown-menu">
		                  <li><a href="#">Action</a></li>
		                  <li><a href="#">Another action</a></li>
		                  <li><a href="#">Something else here</a></li>
		                  <li class="divider"></li>
		                  <li class="nav-header">Nav header</li>
		                  <li><a href="#">Separated link</a></li>
		                  <li><a href="#">One more separated link</a></li>
		                </ul>
		              </li>

			          

			      </ul>
			    </div>

			  </div>
			</div>
		</div> -->

		<div class="container">