<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpresstraining_task4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';/NsHH9Wn2{J)KgO=>F1(G14.D{:]1/iKCg:]NQ36ntUCYR%yFaz*|1[=2[Q+ZrX');
define('SECURE_AUTH_KEY',  'NJy,~$3vKHZ:jo15Z~.$110@Zxhq%A/7PxL(l)IUcuL03~bNH@Hza@lA(Mkd$[6k');
define('LOGGED_IN_KEY',    ' c(UEGivI:k{p{m2(q&:7REZ5SMZ/Jh_#LQrx`:7d%8_-AT9]NgI)M+/#&v Xu|T');
define('NONCE_KEY',        '>pcP)U0tM)7.Z`qZuF,r?svBB% gb!fegx7RDYhr15KG7ubK/?vGLm?%M^~KOGA|');
define('AUTH_SALT',        '+(SaQ>bVld8Ic0/J3Z1mO}j16mHOPU@`h_X0jy};C*6%?u??iwdRB^QZH-f(<{Oj');
define('SECURE_AUTH_SALT', 'RU~#7$3Ov.s^ar2uC&)%7pYrT;cNehlM}M~Ef$XWWiQ;kT0!D42e3Pk=bsr.tH})');
define('LOGGED_IN_SALT',   'CjNK[ww,Z8&f,2tFIf1K=;*he*T++TVASuC|(>eT.znNwg5[u8$5!h,I#ZPk9.SQ');
define('NONCE_SALT',       '.si13Z^|&4tOB]6nqeYO+d?=@Q4$CW=(elSegQfm*,fzJN4?HXYGd{BAU/H]a[Fl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/wordpresstraining_task4/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
